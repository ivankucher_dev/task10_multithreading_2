package com.epam.trainings.mvc.model.blockingqueue;

import com.epam.trainings.mvc.commands.Command;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueCommand implements Command {
  @Override
  public void execute() {
    BlockingQueue<Integer> queue = new LinkedBlockingQueue<>(10);
    new Thread(new Customer(queue)).start();
    new Thread(new EpamSystems(queue)).start();
  }
}
