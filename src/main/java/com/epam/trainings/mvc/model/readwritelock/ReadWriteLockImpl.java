package com.epam.trainings.mvc.model.readwritelock;

import java.util.concurrent.atomic.AtomicInteger;

public class ReadWriteLockImpl implements ReadWriteLock {
  private AtomicInteger readers;
  private AtomicInteger writers;
  private final ReadWriteLockImpl.ReadLock readLock;
  private final ReadWriteLockImpl.WriteLock writeLock;
  private Object sync = new Object();

  @Override
  public ReadWriteLockImpl.WriteLock writeLock() {
    return writeLock;
  }

  @Override
  public ReadWriteLockImpl.ReadLock readLock() {
    return readLock;
  }

  public ReadWriteLockImpl() {
    readers = new AtomicInteger(0);
    writers = new AtomicInteger(0);
    readLock = new ReadLock();
    writeLock = new WriteLock();
  }

  public class WriteLock {
    public void lock() {
      synchronized (sync) {
        if (writers.get() == 0 && readers.get() == 0) {
          writers.incrementAndGet();
        } else {
          try {
            sync.wait();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }

    public void unlock() {
      synchronized (sync) {
        writers.decrementAndGet();
        sync.notify();
      }
    }
  }

  public class ReadLock {
    public void lock() {
      synchronized (sync) {
        if (writers.get() == 0) {
          readers.incrementAndGet();
        } else {
          try {
            sync.wait();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    }

    public void unlock() {
      synchronized (sync) {
        readers.decrementAndGet();
        if (readers.get() == 0) {
          sync.notify();
        }
      }
    }
  }
}
