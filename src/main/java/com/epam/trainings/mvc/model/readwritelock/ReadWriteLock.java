package com.epam.trainings.mvc.model.readwritelock;

interface ReadWriteLock {
  ReadWriteLockImpl.WriteLock writeLock();

  ReadWriteLockImpl.ReadLock readLock();
}
