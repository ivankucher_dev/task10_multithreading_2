package com.epam.trainings.mvc.model.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class EpamSystems implements Runnable {
  private BlockingQueue<Integer> queue;

  EpamSystems(BlockingQueue e) {
    queue = e;
  }

  @Override
  public void run() {
    try {
      while (true) {
        int take = queue.take();
        printTaken(take);
      }
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  private void printTaken(Integer take) throws InterruptedException {
    System.out.println("Epam Systems Take : " + take);
    Thread.sleep(200);
  }
}
