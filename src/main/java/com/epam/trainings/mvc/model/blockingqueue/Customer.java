package com.epam.trainings.mvc.model.blockingqueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;

public class Customer implements Runnable {
  private static Logger log = LogManager.getLogger(Customer.class.getName());
  private BlockingQueue<Integer> queue;

  Customer(BlockingQueue<Integer> queue) {
    this.queue = queue;
  }

  @Override
  public void run() {
    try {
      putSomeValues();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void putSomeValues() throws InterruptedException {
    for (int i = 0; i < 20; i++) {
      queue.put(i);
      log.info("Put " + i + " in queue");
      log.info("Capacity of queue " + queue.remainingCapacity());
      Thread.sleep(100);
    }
  }
}
