package com.epam.trainings.mvc.model.readwritelock;

import java.util.ArrayList;

public class TestCustomRWLock {
  private ArrayList<String> array;
  private final ReadWriteLock rw = new ReadWriteLockImpl();
  private final ReadWriteLockImpl.ReadLock readLock = rw.readLock();
  private final ReadWriteLockImpl.WriteLock writeLock = rw.writeLock();

  public TestCustomRWLock() {
    array = new ArrayList<>();
    initArray();
  }

  public void threadLockTest() {
    new Thread(this::readFromArray).start();
    new Thread(this::readFromArray).start();
    new Thread(this::writeIntoArray).start();
    new Thread(this::readFromArray).start();
  }

  public void readFromArray() {
    try {
      readLock.lock();
      for (String text : array) {
        System.out.println("[Read text] : " + text);
        sleepThread(500);
      }
    } finally {

      readLock.unlock();
    }
  }

  public void writeIntoArray() {
    String text = "write";
    writeLock.lock();
    for (int i = 0; i < 5; i++) {
      array.add(text);
      System.out.println("[Write text] : " + text);
      sleepThread(500);
    }
    writeLock.unlock();
  }

  private void initArray() {
    for (int i = 0; i < 10; i++) {
      array.add("test " + i);
    }
  }

  public void sleepThread(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
