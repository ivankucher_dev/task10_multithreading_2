package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.readwritelock.TestCustomRWLock;

public class RWLockCommand implements Command {
  @Override
  public void execute() {
    TestCustomRWLock test = new TestCustomRWLock();
    test.threadLockTest();
  }
}
