package com.epam.trainings.mvc.commands;

public interface Command {
  void execute();
}
