package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.ReentrantLock;

public class SyncMonitorsCommand implements Command {

  private static Logger log = LogManager.getLogger(SyncMonitorsCommand.class.getName());
  private static ReentrantLock lock = new ReentrantLock();

  public void execute() {
    new Thread(this::printA).start();
    new Thread(this::printB).start();
    new Thread(this::printC).start();
  }

  public void printA() {
    lock.lock();
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        log.error(e);
        e.printStackTrace();
      }
      lock.unlock();
      log.info("sync fist monitor");

  }

  public void printB() {
lock.lock();
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        log.error(e);
        e.printStackTrace();
      }
      lock.unlock();
      log.info("sync second monitor");
    }


  public void printC() {
    lock.lock();
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        log.error(e);
        e.printStackTrace();
      }
      lock.unlock();
      log.info("sync third monitor");

  }
}
